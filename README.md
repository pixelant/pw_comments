# pw_comments

The full documentation can be found here:

https://docs.typo3.org/typo3cms/extensions/pw_comments/


## DDEV setup

A DDEV configuration is shipped with pw_comments.

```
$ ddev start
```

- TYPO3 frontend: https://pw-comments.ddev.site/
- MailHog: http://pw-comments.ddev.site:8025/


### Credentials

- **For TYPO3:** *admin* / *password* (also install tool password)


### Render documentation

Render pw_comments documentation and launch it in local browser:

```
$ ddev docs 
$ ddev docs-launch 
```

